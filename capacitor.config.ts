import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-project-top-app',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
