import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { trendingUpOutline, colorWandOutline, personOutline } from 'ionicons/icons';
import ProfileTab from './pages/ProfileTab';
import TopsTab from './pages/TopsTab';
import GestionTab from './pages/GestionTab';

import './app.css';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path="/profil">
            <ProfileTab />
          </Route>
          <Route exact path="/tops">
            <TopsTab />
          </Route>
          <Route path="/gestion">
            <GestionTab />
          </Route>
          <Route exact path="/">
            <Redirect to="/tops" />
          </Route>
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="profil" href="/profil">
            <IonIcon icon={personOutline} />
            <IonLabel>Profil</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab2" href="/tops">
            <IonIcon icon={trendingUpOutline} />
            <IonLabel>Tops</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab3" href="/gestion">
            <IonIcon icon={colorWandOutline} />
            <IonLabel>Gestion</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
