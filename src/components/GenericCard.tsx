import {IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent} from '@ionic/react';
import './genericCard.css';

export interface ContainerProps {
  title: string,
  description: string,
  link: string;
}

const GenericCard: React.FC<ContainerProps> = ({ title, description, link }) => {
  return (
    <IonCard>
      <IonCardHeader>
        <div className="generic-card-header">
          <IonCardTitle>{title}</IonCardTitle>
          <p className="generic-card-header-stat">Items : 0</p>
        </div>
      </IonCardHeader>
      <IonCardContent>
        <p className="generic-card-content-description">{description}</p>
        <div className="generic-card-content-link-container">
          <IonCardSubtitle>
            <a className="generic-card-link" href={link}>Voir plus &#xbb;</a>
          </IonCardSubtitle>
        </div>
      </IonCardContent>
    </IonCard>
  );
};

export default GenericCard;
