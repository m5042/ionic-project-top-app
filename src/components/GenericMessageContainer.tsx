import './genericMessageContainer.css';

interface ContainerProps {
  name: string,
  link: string;
}

const GenericMessageContainer: React.FC<ContainerProps> = ({ name, link }) => {
  return (
    <div className="container">
      {name !== "" ? <strong>{name}</strong> : ""}
      {
        link === "/gestion" && <a href={link}>Ajouter un top</a>
      }
    </div>
  );
};

export default GenericMessageContainer;
