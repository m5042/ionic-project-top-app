import {Fragment} from 'react';
import './profilInfo.css';

interface ContainerProps {
  name: string,
  data: string;
}

const GenericMessageContainer: React.FC<ContainerProps> = ({ name, data }) => {
  return (
    <Fragment>
      <div className="separator"></div>
      <p className="info-name">{name}</p>
      <p className="info-data">{data}</p>
    </Fragment>
  );
};

export default GenericMessageContainer;
