import {useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonIcon, IonModal } from '@ionic/react';
import { colorWandOutline, addCircleOutline, brushOutline, trashOutline } from 'ionicons/icons';
import GenericMessageContainer from '../components/GenericMessageContainer';
import './gestionTab.css';

const GestionTab: React.FC = () => {

  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);

  return (
    <IonPage>
      <IonContent fullscreen>
        <div className="gestion-content-container">
          <p className="gestion-content-title section-title">Gestion des tops</p>

          <div className="gestion-content-add-form">
            <p className="gestion-form-text text-center">Cette page vous permet de créer de nouveaux tops, d'en supprimer ou encore de modifier vos tops existants.</p>

            <button className="form-button add-form-button" onClick={() => setIsAddModalVisible(true)}><IonIcon className="ion-icon" icon={addCircleOutline}/><span className="form-button-span">Ajouter un Top</span></button>
            <button className="form-button edit-form-button" onClick={() => setIsEditModalVisible(true)}><IonIcon className="ion-icon" icon={brushOutline}/><span className="form-button-span">Editer un Top</span></button>
            <button className="form-button delete-form-button" onClick={() => setIsDeleteModalVisible(true)}><IonIcon className="ion-icon" icon={trashOutline}/><span className="form-button-span">Supprimer un Top</span></button>
          </div>
        </div>

        {/* ADD TOP MODAL */}
        <IonModal isOpen={isAddModalVisible}>
          <div className="modal-content-container">
            <p>Add modal content</p>
          </div>
          <button className="modal-button" onClick={() => setIsAddModalVisible(false)}>Fermer</button>
        </IonModal>

        {/* EDIT TOP MODAL */}
        <IonModal isOpen={isEditModalVisible}>
          <div className="modal-content-container">
            <p>Edit modal content</p>
          </div>
          <button className="modal-button" onClick={() => setIsEditModalVisible(false)}>Fermer</button>
        </IonModal>

        {/* DELETE TOP MODAL */}
        <IonModal isOpen={isDeleteModalVisible}>
          <div className="modal-content-container">
            <p>Delete modal content</p>
          </div>
          <button className="modal-button" onClick={() => setIsDeleteModalVisible(false)}>Fermer</button>
        </IonModal>
      </IonContent>
    </IonPage>
  );
};

export default GestionTab;
