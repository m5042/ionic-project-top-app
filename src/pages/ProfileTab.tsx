import {Fragment} from 'react';
import {useEffect, useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonIcon } from '@ionic/react';
import { personOutline, powerOutline, shareSocialOutline } from 'ionicons/icons';
import GenericMessageContainer from '../components/GenericMessageContainer';
import ProfilInfo from '../components/ProfilInfo';
import './profileTab.css';

export interface Iuser {
  firstname: string,
  lastname: string,
  type: string,
}

const ProfileTab: React.FC = () => {

  useEffect(() => {
    fetch(``)
  }, []);

  const [user, setUser] = useState([
    {
      firstname:"Enzo",
      lastname: "Avagliano",
      type: "Member",
      infos: [
        {
          name: "Email",
          data: "enzo.avagliano@ynov.com"
        },
        {
          name: "Date de naissance",
          data: "09/11/2000"
        },
        {
          name: "Site web",
          data: "https://enzoavagliano.fr"
        }
      ]
    }
  ]);

  return (
    <IonPage>
      {/* <IonHeader>
        <IonToolbar>
          <IonTitle><strong><IonIcon icon={personOutline}/> Votre profil</strong></IonTitle>
        </IonToolbar>
      </IonHeader> */}
      <IonContent fullscreen>
        <div className="profile-tab-content">
          <img className="profil-avatar" src="./assets/img/default_avatar.png" alt="Default Avatar"/>
          <p className="profil-name">{user[0].firstname} {user[0].lastname}</p>
          <p className="profil-type">{user[0].type}</p>

          <div className="profil-links-container">
            {
              user[0].infos.map((item, index) => {
                return(
                  <ProfilInfo name={item.name} data={item.data}/>
                )
              })
            }
            <div className="separator"></div>
          </div>
          <div className="profil-cta-container">
            <button className="button button-share"><IonIcon icon={shareSocialOutline}/> Partager mon profil</button>
            <button className="button button-logout"><IonIcon icon={powerOutline}/> Déconnexion</button>
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default ProfileTab;
