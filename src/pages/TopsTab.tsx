import {useEffect, useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonIcon } from '@ionic/react';
import { trendingUpOutline } from 'ionicons/icons';
import GenericMessageContainer from '../components/GenericMessageContainer';
import GenericCard from '../components/GenericCard';
import {ContainerProps} from '../components/GenericCard';
import './topsTab.css';

const Tab2: React.FC = () => {

  const [topsList, setTopsList] = useState([{title: "Kpop girls group", description:"Le meilleur des groupes de Kpop féminin.", link:"/profile"}]);

  return (
    <IonPage>
      {/* <IonHeader>
        <IonToolbar>
          <IonTitle><strong><IonIcon icon={trendingUpOutline}/> Vos tops</strong></IonTitle>
        </IonToolbar>
      </IonHeader> */}
      <IonContent fullscreen>
        <div className="tops-content">
          <div className="tops-content-title-container">
            <p className="gestion-content-title section-title">Votre liste de tops</p>
          </div>
          {
            topsList.length === 0 && <GenericMessageContainer name="Aucun top trouvé" link="/gestion" />
          }
          {
            topsList. length > 0 &&
            topsList.map((item:ContainerProps, index:number) => {
              return(
                <GenericCard key={index} title={item.title} description={item.description} link={item.link}/>
              )
            })
          }
          {
            topsList.length > 0 && <GenericMessageContainer name="" link="/gestion" />
          }
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
